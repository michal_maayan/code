/**
 * helper functions for the generic DFS process
 */

#include "SudokuTree.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define SUCCESS 0
#define FAILED  1
#define MAX_PARAMETER_IN_LINE 100

/**
 * calculate how many non empty cells there are in the board, by counting all the
 * cells containing non 0 value;
 * @param p represents a pointer to sudoku board
 * @return the number of non zero cells
 */
unsigned int getVal(pNode p)
{
    assert(p != NULL);
    SudokuBoard *givenNode = ((SudokuBoard *) p);
    unsigned int nonZeroCounter = 0;
    for (int i = 0; i < givenNode->nParam; i++)
    {
        for (int j = 0; j < givenNode->nParam; j++)
        {
            if (givenNode->array[i][j])
            {
                nonZeroCounter++;
            }
        }
    }
    return nonZeroCounter;
}

/**
 * check if a given number not find int he row
 * @param board the board
 * @param numToCheck the num to look for
 * @param rowNum the row
 * @return Failed if the num found in the line and Success otherwise
 */
int checkRow(SudokuBoard *board, unsigned int numToCheck, unsigned int rowNum)
{
    assert(board != NULL);
    for (int i = 0; i < board->nParam; ++i)
    {
        if ((unsigned int) board->array[rowNum][i] == numToCheck)
        {
            return FAILED;
        }
    }
    return SUCCESS;
}

/**
 * check if a given number not find in the col
 * @param board the board
 * @param numToCheck the num to look for
 * @param colNum the column
 * @return Failed if the num found in the line and Success otherwise
 */
int checkCol(SudokuBoard *board, unsigned int numToCheck, unsigned int colNum)
{
    assert(board != NULL);
    for (int i = 0; i < board->nParam; ++i)
    {
        if ((unsigned int) board->array[i][colNum] == numToCheck)
        {
            return FAILED;
        }
    }
    return SUCCESS;
}

/**
 * check if a given number not find in the square
 * @param board the board
 * @param numToCheck the num to look for
 * @param row the row num
 * @param col the col num
 * @return Failed if the num found in the line and Success otherwise
 */
int checkSquare(SudokuBoard *board, unsigned int numToCheck, unsigned int row, unsigned int col)
{
    assert(board != NULL);
    int squareWidth = (int) sqrt(board->nParam);
    int startRow = squareWidth * (row / squareWidth);
    int startCol = squareWidth * (col / squareWidth);
    for (int i = startRow; i < startRow + squareWidth; ++i)
    {
        for (int j = startCol; j < startCol + squareWidth; ++j)
        {
            if ((unsigned int) board->array[i][j] == numToCheck)
            {
                return FAILED;
            }
        }
    }
    return SUCCESS;
}

/**
 * look for empty cells in the board (cells with zero)
 * @param givenNode represnts a board
 * @param row a pointer to the row num
 * @param col a pointer to the col num
 * @return Success if an empty cell found and Failed otherwise
 */
int checkEmptySpace(SudokuBoard *givenNode, unsigned int *row, unsigned int *col)
{
    assert(givenNode != NULL);
    for (int i = 0; i < givenNode->nParam; ++i)
    {
        for (int j = 0; j < givenNode->nParam; ++j)
        {
            // empty space
            if (givenNode->array[i][j] == 0)
            {
                *row = (unsigned int) i;
                *col = (unsigned int) j;
                return SUCCESS;
            }
        }
    }
    return FAILED;
}

/**
 * deep copy to a node represents a board
 * @param p represents a pointer to sudoku board
 * @return the node that was copied or NULL if there was a problem
 */
pNode copyNode(pNode p)
{
    assert(p != NULL);
    SudokuBoard *givenNode = (SudokuBoard *) p;
    SudokuBoard *copied = (SudokuBoard *) malloc(sizeof(*givenNode));
    if (copied == NULL)
    {
        fprintf(stderr, "out of memory, cannot create new struct \n");
        return NULL;
    }
    copied->nParam = givenNode->nParam;
    copied->array = malloc(givenNode->nParam * sizeof(int *));
    if (copied->array == NULL)
    {
        fprintf(stderr, "out of memory, cannot create a new array \n");
        return NULL;
    }
    for (int i = 0; i < givenNode->nParam; ++i)
    {
        copied->array[i] = (int *) malloc(givenNode->nParam * sizeof(int));
        for (int j = 0; j < givenNode->nParam; ++j)
        {
            copied->array[i][j] = givenNode->array[i][j];
        }
    }
    return copied;
}

/**
 * check if a board os valid according to the soduku rules
 * @param givenNode a node represents a board
 * @param numToCheck to check
 * @param row row
 * @param col col
 * @return Success if the Board is valid and Failed otherwise
 */
int validSodukuParam(SudokuBoard *givenNode, unsigned int numToCheck, unsigned int row,
                     unsigned int col)
{
    int validRow = checkRow(givenNode, numToCheck, row);
    int validCOl = checkCol(givenNode, numToCheck, col);
    int validSquare = checkSquare(givenNode, numToCheck, row, col);
    if (validRow == SUCCESS && validCOl == SUCCESS && validSquare == SUCCESS)
    {
        return SUCCESS;
    }
    return FAILED;
}

/**
 * check how many valid foloowed board a given board has
 * @param p the board
 * @param listOfNodes the list to it insert the valid children
 * @return number of children
 */
int getChildren(pNode p, pNode **listOfNodes)
{
    assert(p != NULL);
    SudokuBoard *givenNode = (SudokuBoard *) p;
    unsigned int row = 0, col = 0, counter = 0;
    if (checkEmptySpace(givenNode, &row, &col) == FAILED)
    {
        //there is no empty spaces
        return counter;
    }
    SudokuBoard *tempChild[MAX_PARAMETER_IN_LINE] = {NULL};
    for (unsigned int i = 1; i <= (unsigned int) givenNode->nParam; ++i)
    {
        if (validSodukuParam(givenNode, i, row, col) == SUCCESS)
        {
            SudokuBoard *copied = (SudokuBoard *) copyNode(givenNode);
            copied->array[row][col] = i;
            tempChild[counter] = copied;
            counter++;
        }
    }
    SudokuBoard **listOfChild = (SudokuBoard **) malloc(sizeof(void *) * counter);
    *listOfNodes = (pNode *) listOfChild;
    if (listOfChild == NULL)
    {
        return 0;
    }
    for (unsigned int j = 0; j < counter; ++j)
    {
        listOfChild[j] = tempChild[j];
    }
    return counter;
}

/**
 * free the node
 * @param p represents a soduku board
 */
void freeNode(pNode p)
{
    SudokuBoard *givenNode = (SudokuBoard *) p;
    if (givenNode != NULL)
    {
        if (givenNode->array != NULL)
        {
            for (int i = 0; i < givenNode->nParam; i++)
            {
                free(givenNode->array[i]);
            }
            free(givenNode->array);
        }
        free(givenNode);
    }
}


