/**
 * solve a soduku game according to a givem board
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "GenericDFS.h"
#include "SudokuTree.h"

#define MAX_PARAMETER_IN_LINE 400
#define MAX_NUM 100
#define FAILED 1
#define SUCCESS 0

/**
* closing the given file, in case of an error the program will be closed
* @param file input file
* @return Failed if there was a failure and Success otherwise
*/
static int closeFile(FILE *file)
{
    if (fclose(file) == EOF)
    {
        fprintf(stderr, "failed with closing file");
        return FAILED;
    }
    return SUCCESS;
}

/**
 * validate that a given string could converted to a long number and that the num
 * is a non negative number
 * @param stringToCheck represents a number
 * @param returnNum the converted number if the converted succeed
 * @return Failed if there was a failure and Success otherwise
  */
static int checkL(char stringToCheck[MAX_PARAMETER_IN_LINE], long *returnNum)
{
    char *checkStr = '\0';
    long int optionalL = 0;
    // check if the user just press enter
    if (stringToCheck[0] == '\n')
    {
        fprintf(stderr, "%s", "error - empty input\n");
        return FAILED;
    }
    // the numbers are in base 10
    optionalL = strtol(stringToCheck, &checkStr, 10);
    // the parameter include only digits or "\r", "\n" chars
    if (*checkStr == 0 || *checkStr == '\n' || *checkStr == '\r')
    {
        //optionalL has to be a non negative number
        if (optionalL < 0)
        {
            fprintf(stderr, "%s", "error - not digit\n");
            return FAILED;
        }
        *returnNum = optionalL;
        return SUCCESS;
    }
    fprintf(stderr, "%s", "error - not digit\n");
    return FAILED;
}

/**
 * check if the number has an integer root
 * @param optionalN the number to check
 * @return the root if it has and -1 otherwise
 */
static int checkRoot(const int optionalN)
{
    if (optionalN == 1)
    {
        return optionalN;
    }
    for (int i = 1; i <= (optionalN / 2); i++)
    {
        if (i * i == optionalN)
        {
            return i;
        }
    }
    return -1;
}

/**
 * initial a board
 * @param n number of rows and columns
 * @param pBoard pointer to the board need ti be initial
 * @return Failed if there was a failure and Success otherwise
 */
static int buildBoard(int n, SudokuBoard *pBoard)
{
    pBoard->nParam = n;
    pBoard->array = (int **) malloc(n * sizeof(int *));
    if (pBoard->array == NULL)
    {
        fprintf(stderr, "out of memory, cannot create stack \n");
        return FAILED;
    }
    for (int i = 0; i < n; i++)
    {
        pBoard->array[i] = (int *) malloc(n * sizeof(int));
        if (pBoard->array[i] == NULL)
        {
            fprintf(stderr, "out of memory, cannot create stack \n");
            // free the location allocated until now
            for (int j = 0; j < i; ++j)
            {
                free(pBoard->array[j]);
            }
            free(pBoard->array);
            return FAILED;
        }
    }
    return SUCCESS;
}

/**
 * trying to read a line from file
 * @param buf the array to insert the read line
 * @param file the file to read
 * @return Failed if there was a failure and Success otherwise
 */
static int validFgets(char *buf, FILE *file)
{
    if (fgets(buf, MAX_PARAMETER_IN_LINE, file) == NULL)
    {
        fprintf(stderr, "failed reading the current line");
        return FAILED;
    }
    return SUCCESS;
}

/**
 * print an error
 */
static int errorInValid()
{
    fprintf(stderr, "%s", "not a valid sudoku file \n");
    return FAILED;
}

/**
 * read a line from the file, validate it and insert it to the board
 * @param file the file to be read
 * @param n number of numbers in line
 * @param buf the array which the line will be insert to
 * @param lineInBoard the line in the board to it the number will be insert
 * @return Failed if there was a failure and Success otherwise
 */
static int readLine(FILE *file, const int n, char *buf, int *lineInBoard)
{
    if (validFgets(buf, file))
    {
        // need to return failed
        return FAILED;
    }
    char *token = strtok(buf, " ");
    int i = 0;
    while (token != NULL)
    {
        // check if there is left more then n-1 parameters in the line
        if (i > (n - 1))
        {
            return errorInValid();
        }
        long optionalNum;
        if (checkL(token, &optionalNum) == FAILED)
        {
            return errorInValid();
        }
        if (optionalNum == -1)
        {
            return errorInValid();
        }
        if (optionalNum > n)
        {
            return errorInValid();
        }
        // insert the number to a specific cell
        lineInBoard[i] = (int) optionalNum;
        token = strtok(NULL, " ");
        i++;
    }
    return SUCCESS;
}

/**
 * read the first line in the file which represents the N parameter.
 * In case of an error reading the line from the file, the file and this program will closed
 * @param file to read
 * @param buf the array which the numbers wil be insert to
 * @param nParam pointer to int
 * @return Failed if there was a failure and Success otherwise
 */
static int checkNParam(FILE *file, char *buf, int *nParam)
{
    if (validFgets(buf, file))
    {
        return FAILED;
    }
    long optionalN;
    checkL(buf, &optionalN);
    if (optionalN == -1 || optionalN == 0)
    {
        fprintf(stderr, "The N param has to be positive number");
        return FAILED;
    }
    if (optionalN > MAX_NUM)
    {
        fprintf(stderr, "The N param has to be smaller the 100");
        return FAILED;
    }
    int isIntRoot = checkRoot((int) optionalN);
    if (isIntRoot == -1)
    {
        return errorInValid();
    }
    *nParam = (int) optionalN;
    return SUCCESS;
}

/**
 *
 * @param finish
 */
static void printBoard(SudokuBoard *finish)
{
    printf("%d\n", finish->nParam);
    for (int i = 0; i < finish->nParam; ++i)
    {
        for (int j = 0; j < finish->nParam; ++j)
        {
            if (j != finish->nParam - 1)
            {
                printf("%d ", finish->array[i][j]);
            }
            else
            {
                printf("%d", finish->array[i][j]);
            }
        }
        printf("\n");
    }
}

/**
 * read the whole file
 * @param file the input file
 */
int readFile(FILE *file)
{
    char buf[MAX_PARAMETER_IN_LINE] = {'\0'};
    int n;
    if (checkNParam(file, buf, &n))
    {
        return FAILED;
    }
    SudokuBoard *pboard = malloc(sizeof(SudokuBoard));
    if (buildBoard(n, pboard))
    {
        return FAILED;
    }
    for (int i = 0; i < n; i++)
    {
        // restart buff with 0
        memset(buf, '\0', MAX_PARAMETER_IN_LINE);
        if (readLine(file, n, buf, pboard->array[i]))
        {
            freeNode(pboard);
            return FAILED;
        }
    }
    // the initial board was created trying to solve the game
    // the maximal number in the board
    unsigned int best = (unsigned int) pboard->nParam * pboard->nParam;
    SudokuBoard *finish = getBest(pboard, getChildren, getVal, freeNode, copyNode, best);
    freeNode(pboard);
    if (finish == NULL)
    {
        printf("no solution");
        return FAILED;
    }
    printBoard(finish);
    freeNode(finish);
    return SUCCESS;
}

int main(int argc, char *argv[])
{
    char *inPut;
    FILE *inputFile;
    if (argc < 2)
    {
        fprintf(stderr, "please supply a file! usage: sudokusolver \n");
        return FAILED;
    }
    if (argc > 2)
    {
        fprintf(stderr, "Too many args \n");
        return FAILED;
    }
    inPut = argv[1];
    inputFile = fopen(inPut, "r");
    if (inputFile == NULL)
    {
        fprintf(stderr, "%s: no such file \n", inPut);
        return FAILED;
    }
    readFile(inputFile);
    return closeFile(inputFile);
}