/**
 * a generic DFS proceess
 */

#include "GenericDFS.h"
#include <stdio.h>
#include <malloc.h>
#include <memory.h>
#include <stdbool.h>

#define SUCCESS 0
#define FAILED  1

/**
 * a generic node on stack
 */
typedef struct Node
{
    void *_data; // pointer to anything
    struct Node *_next;
} Node;

/**
 * stack of generic nodes
 */
typedef struct Stack
{
    Node *_top;
    int counterStack;
} Stack;

/**
 * alloe memory to a new stack
 * @return new stack on the heap
 */
Stack *stackAlloc()
{
    Stack *stack = (Stack *) malloc(sizeof(Stack));
    if (stack == NULL)
    {
        fprintf(stderr, "not enough memory \n");
        return NULL;
    }
    stack->_top = NULL;
    stack->counterStack = 0;
    return stack;
}

/**
 * free the stack
 * @param stack free the stack
 * @param freeNode a function that free a node
 */
void freeStack(Stack **stack, freeNodeFunc freeNode)
{
    Node *p1;
    Node *p2;
    if ((*stack != NULL))
    {
        p1 = (*stack)->_top;
        // free rest of the boards from the stack
        while (p1)
        {
            p2 = p1;
            p1 = p1->_next;
            freeNode(p2->_data);
            free(p2);
        }
        free(*stack);
        *stack = NULL;
    }
}

/**
 * push another node to the stack
 * @param stack of nodes
 * @param data the value of the pushed node
 * @param copy a function that copy data
 * @return Success if the push command succeded and Failed otherwise
 */
int push(Stack *stack, pNode data, copyNodeFunc copy)
{
    Node *node = (Node *) malloc(sizeof(Node));
    if (node == NULL)
    {
        fprintf(stderr, "not enough memory \n");
        return FAILED;
    }
    node->_data = copy(data);
    node->_next = stack->_top;
    stack->_top = node;
    stack->counterStack++;
    return SUCCESS;
}

/**
 * pop the top node in the stack
 * @param stack of nodes
 * @return pointer to the popped node
 */
pNode pop(Stack *stack)
{
    if (stack == NULL)
    {
        fprintf(stderr, "not enough memory \n");
        return NULL;
    }
    if (stack->_top == NULL)
    {
        fprintf(stderr, "stack is empty \n");
        return NULL;
    }
    stack->counterStack--;
    Node *node = stack->_top;
    stack->_top = node->_next;
    pNode ptr = node->_data;
    free(node);
    return ptr;
}

/**
 * @brief getBest This function returns the best valued node in a tree using
 * DFS algorithm.
 * @param head The head of the tree
 * @param getChildren A function that gets a node and a pointer to array of nodes.
 * the function allocates memory for an array of all the children of the node, and
 * returns the number of the children.
 * (the array might be initialized, but the function can't know how many children
 * the node has)
 * @param getVal A function that gets a node and returns its value, as int
 * @param freeNode A function that frees node from memory.
 * this function will be called for each Node returns by getChilds.
 * @param best The best available value for a node, when the function encounters
 * a node with that value it stops looking and returns it.
 * If the best value can't be determined, pass UINT_MAX (defined in limits.h)
 * for that param.
 * @param copy A function that do deep copy of Node.
 * @return The best valued node in the tree
 * In case of an error, or when all the nodes in the tree valued zero the returns
 * Node is NULL.
 * If some nodes shares the best valued, the function returns the first one it encounters.
 */
pNode getBest(pNode head, getNodeChildrenFunc getChildren,
              getNodeValFunc getVal, freeNodeFunc freeNode, copyNodeFunc copy, unsigned int best)
{
    //todo just for debug
//    static int round = 0;
    if (head == NULL)
    {
        fprintf(stderr, "head is NULL \n");
        return NULL;
    }
    // the soduku board is complete
    if (getVal(head) == best)
    {
        return copy(head);
    }
    pNode maxNode = copy(head);
    Stack *stack = stackAlloc();
    if (stack == NULL)
    {
        freeNode(maxNode);
        return NULL;
    }
    if (push(stack, head, copy) == FAILED)
    {
        freeStack(&stack, freeNode);
        freeNode(maxNode);
        return NULL;
    }
    bool isFound = false;
    while (isFound == false || stack->counterStack > 0)
    {
        pNode *arrayOfNodes = NULL;
        pNode temp = pop(stack);
        if (temp == NULL)
        {
            freeNode(maxNode);
            freeStack(&stack, freeNode);
            return NULL;
        }
        // in case the temp node has the best value, return the node and change the flag
        if (getVal(temp) == best)
        {
            freeNode(maxNode);
            maxNode = copy(temp);
            isFound = true;
        }
        // DFS process
        if (isFound == false)
        {
            int numOfChildren = getChildren(temp, &arrayOfNodes);
            // insert children as requested (handle the first children first)
            for (int i = (numOfChildren - 1); i >= 0; i--)
            {
                if (push(stack, arrayOfNodes[i], copy) == FAILED)
                {
                    freeNode(maxNode);
                    freeNode(temp);
                    free(arrayOfNodes);
                    freeStack(&stack, freeNode);
                    return NULL;
                }
                freeNode(arrayOfNodes[i]);
            }
        }
        freeNode(temp);
        free(arrayOfNodes);
    }

    // Freeing the stack
    freeStack(&stack, freeNode);
    if (isFound)
    {
        return maxNode;
    }
    // No solution was found.
    return NULL;
}