//
// Created by micim on 18-Aug-17.
//

#ifndef CODE_SUDOKUTREE_H
#define CODE_SUDOKUTREE_H

#include "GenericDFS.h"

/**
 * a struct represents a soduku board
 */
typedef struct SudokuBoard
{
    int nParam;
    int **array;
} SudokuBoard;

/**
 * calculate how many non empty cells there are in the board, by counting all the
 * cells containing non 0 value;
 * @param p represents a pointer to sudoku board
 * @return the number of non zero cells
 */
unsigned int getVal(pNode p);

/**
 * deep copy to a node represents a board
 * @param p represents a pointer to sudoku board
 * @return the node that was copied or NULL if there was a problem
 */
pNode copyNode(pNode p);

/**
 * check how many valid foloowed board a given board has
 * @param p the board
 * @param listOfNodes the list to it insert the valid children
 * @return number of children
 */
int getChildren(pNode p, pNode **listOfNodes);

/**
 * free the node
 * @param p represents a soduku board
 */
void freeNode(pNode p);


#endif //CODE_SUDOKUTREE_H
